--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)

-- Started on 2019-07-31 02:30:35 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 25433)
-- Name: pizzaocpublicschema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA pizzaocpublicschema;


ALTER SCHEMA pizzaocpublicschema OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 206 (class 1259 OID 25505)
-- Name: adresse; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.adresse (
    adresse_id_table integer NOT NULL,
    client_id_adresse integer NOT NULL,
    code_postal integer NOT NULL,
    ville character varying NOT NULL,
    adresse character varying NOT NULL,
    adresse_numero integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.adresse OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 25447)
-- Name: article; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.article (
    article_id integer NOT NULL,
    carte_id_article integer NOT NULL,
    name_article character varying NOT NULL,
    type_id_article integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.article OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 25463)
-- Name: article_stock_association_table; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.article_stock_association_table (
    article_stock_association_id_table integer NOT NULL,
    stock_id_assocaition integer NOT NULL,
    article_id_association integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.article_stock_association_table OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 25442)
-- Name: carte; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.carte (
    carte_id_table integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.carte OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 25489)
-- Name: client; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.client (
    client_id integer NOT NULL,
    nom_client character varying NOT NULL,
    prenom_client character varying NOT NULL
);


ALTER TABLE pizzaocpublicschema.client OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 25497)
-- Name: coordonnes; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.coordonnes (
    coordonnes_id_client integer NOT NULL,
    telephone integer NOT NULL,
    mail character varying NOT NULL,
    client_id_coordonnes integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.coordonnes OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 25473)
-- Name: employe; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.employe (
    pizzeria_id_employe integer NOT NULL,
    employe_id real NOT NULL,
    nom_employe character varying NOT NULL,
    statut_employe character varying NOT NULL,
    disponible_travail character varying NOT NULL
);


ALTER TABLE pizzaocpublicschema.employe OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 25518)
-- Name: ligne_panier; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.ligne_panier (
    ligne_panier_id integer NOT NULL,
    panier_id_ligne_panier integer NOT NULL,
    article_id_ligne_panier integer NOT NULL,
    quantite_ligne_article integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.ligne_panier OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 25481)
-- Name: mode_transport; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.mode_transport (
    transport_id_client integer NOT NULL,
    mode_transport_livraison character varying NOT NULL
);


ALTER TABLE pizzaocpublicschema.mode_transport OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 25513)
-- Name: panier; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.panier (
    panier_id integer NOT NULL,
    client_id_panier integer NOT NULL,
    transport_id_panier integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.panier OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 25468)
-- Name: pizzeria; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.pizzeria (
    pizzeria_id integer NOT NULL,
    carte_id integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.pizzeria OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 25455)
-- Name: stock; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.stock (
    stock_id_stock integer NOT NULL,
    produit_name_stock character varying NOT NULL,
    produit_quantite_stock integer NOT NULL
);


ALTER TABLE pizzaocpublicschema.stock OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 25434)
-- Name: type; Type: TABLE; Schema: pizzaocpublicschema; Owner: postgres
--

CREATE TABLE pizzaocpublicschema.type (
    type_id_table integer NOT NULL,
    name_type_article character varying NOT NULL
);


ALTER TABLE pizzaocpublicschema.type OWNER TO postgres;

--
-- TOC entry 2884 (class 2606 OID 25512)
-- Name: adresse adresse_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.adresse
    ADD CONSTRAINT adresse_pk PRIMARY KEY (adresse_id_table);


--
-- TOC entry 2868 (class 2606 OID 25454)
-- Name: article article_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article
    ADD CONSTRAINT article_pk PRIMARY KEY (article_id);


--
-- TOC entry 2872 (class 2606 OID 25467)
-- Name: article_stock_association_table article_stock_association_table_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article_stock_association_table
    ADD CONSTRAINT article_stock_association_table_pk PRIMARY KEY (article_stock_association_id_table, stock_id_assocaition, article_id_association);


--
-- TOC entry 2866 (class 2606 OID 25446)
-- Name: carte carte_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.carte
    ADD CONSTRAINT carte_pk PRIMARY KEY (carte_id_table);


--
-- TOC entry 2880 (class 2606 OID 25496)
-- Name: client client_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.client
    ADD CONSTRAINT client_pk PRIMARY KEY (client_id);


--
-- TOC entry 2882 (class 2606 OID 25504)
-- Name: coordonnes coordonnes_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.coordonnes
    ADD CONSTRAINT coordonnes_pk PRIMARY KEY (coordonnes_id_client);


--
-- TOC entry 2876 (class 2606 OID 25480)
-- Name: employe employe_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.employe
    ADD CONSTRAINT employe_pk PRIMARY KEY (pizzeria_id_employe, employe_id);


--
-- TOC entry 2888 (class 2606 OID 25522)
-- Name: ligne_panier ligne_panier_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.ligne_panier
    ADD CONSTRAINT ligne_panier_pk PRIMARY KEY (ligne_panier_id, panier_id_ligne_panier, article_id_ligne_panier);


--
-- TOC entry 2878 (class 2606 OID 25488)
-- Name: mode_transport mode_transport_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.mode_transport
    ADD CONSTRAINT mode_transport_pk PRIMARY KEY (transport_id_client);


--
-- TOC entry 2886 (class 2606 OID 25517)
-- Name: panier panier_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.panier
    ADD CONSTRAINT panier_pk PRIMARY KEY (panier_id);


--
-- TOC entry 2874 (class 2606 OID 25472)
-- Name: pizzeria pizzeria_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.pizzeria
    ADD CONSTRAINT pizzeria_pk PRIMARY KEY (pizzeria_id);


--
-- TOC entry 2870 (class 2606 OID 25462)
-- Name: stock stock_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.stock
    ADD CONSTRAINT stock_pk PRIMARY KEY (stock_id_stock);


--
-- TOC entry 2864 (class 2606 OID 25441)
-- Name: type type_pk; Type: CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.type
    ADD CONSTRAINT type_pk PRIMARY KEY (type_id_table);


--
-- TOC entry 2891 (class 2606 OID 25543)
-- Name: article_stock_association_table article_article_stock_association_table_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article_stock_association_table
    ADD CONSTRAINT article_article_stock_association_table_fk FOREIGN KEY (article_id_association) REFERENCES pizzaocpublicschema.article(article_id);


--
-- TOC entry 2899 (class 2606 OID 25538)
-- Name: ligne_panier article_ligne_panier_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.ligne_panier
    ADD CONSTRAINT article_ligne_panier_fk FOREIGN KEY (article_id_ligne_panier) REFERENCES pizzaocpublicschema.article(article_id);


--
-- TOC entry 2890 (class 2606 OID 25533)
-- Name: article carte_article_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article
    ADD CONSTRAINT carte_article_fk FOREIGN KEY (carte_id_article) REFERENCES pizzaocpublicschema.carte(carte_id_table);


--
-- TOC entry 2893 (class 2606 OID 25528)
-- Name: pizzeria carte_pizzeria_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.pizzeria
    ADD CONSTRAINT carte_pizzeria_fk FOREIGN KEY (carte_id) REFERENCES pizzaocpublicschema.carte(carte_id_table);


--
-- TOC entry 2896 (class 2606 OID 25568)
-- Name: adresse client_adresse_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.adresse
    ADD CONSTRAINT client_adresse_fk FOREIGN KEY (client_id_adresse) REFERENCES pizzaocpublicschema.client(client_id);


--
-- TOC entry 2895 (class 2606 OID 25573)
-- Name: coordonnes client_coordonnes_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.coordonnes
    ADD CONSTRAINT client_coordonnes_fk FOREIGN KEY (client_id_coordonnes) REFERENCES pizzaocpublicschema.client(client_id);


--
-- TOC entry 2898 (class 2606 OID 25563)
-- Name: panier client_panier_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.panier
    ADD CONSTRAINT client_panier_fk FOREIGN KEY (client_id_panier) REFERENCES pizzaocpublicschema.client(client_id);


--
-- TOC entry 2897 (class 2606 OID 25558)
-- Name: panier mode_transport_panier_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.panier
    ADD CONSTRAINT mode_transport_panier_fk FOREIGN KEY (transport_id_panier) REFERENCES pizzaocpublicschema.mode_transport(transport_id_client);


--
-- TOC entry 2900 (class 2606 OID 25578)
-- Name: ligne_panier panier_ligne_panier_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.ligne_panier
    ADD CONSTRAINT panier_ligne_panier_fk FOREIGN KEY (panier_id_ligne_panier) REFERENCES pizzaocpublicschema.panier(panier_id);


--
-- TOC entry 2894 (class 2606 OID 25553)
-- Name: employe pizzeria_employe_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.employe
    ADD CONSTRAINT pizzeria_employe_fk FOREIGN KEY (pizzeria_id_employe) REFERENCES pizzaocpublicschema.pizzeria(pizzeria_id);


--
-- TOC entry 2892 (class 2606 OID 25548)
-- Name: article_stock_association_table stock_article_stock_association_table_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article_stock_association_table
    ADD CONSTRAINT stock_article_stock_association_table_fk FOREIGN KEY (stock_id_assocaition) REFERENCES pizzaocpublicschema.stock(stock_id_stock);


--
-- TOC entry 2889 (class 2606 OID 25523)
-- Name: article type_article_fk; Type: FK CONSTRAINT; Schema: pizzaocpublicschema; Owner: postgres
--

ALTER TABLE ONLY pizzaocpublicschema.article
    ADD CONSTRAINT type_article_fk FOREIGN KEY (type_id_article) REFERENCES pizzaocpublicschema.type(type_id_table);


-- Completed on 2019-07-31 02:30:35 CEST

--
-- PostgreSQL database dump complete
--

