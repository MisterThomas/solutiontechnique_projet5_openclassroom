-- SCHEMA: publicocschema

-- DROP SCHEMA publicocschema ;

CREATE SCHEMA pizzaocpublicschema
    AUTHORIZATION postgres;


CREATE TABLE pizzaocpublicschema.type (
                type_id_table INTEGER NOT NULL,
                name_type_article VARCHAR NOT NULL,
                CONSTRAINT type_pk PRIMARY KEY (type_id_table)
);


CREATE TABLE pizzaocpublicschema.Carte (
                carte_id_table INTEGER NOT NULL,
                CONSTRAINT carte_pk PRIMARY KEY (carte_id_table)
);


CREATE TABLE pizzaocpublicschema.Article (
                article_id INTEGER NOT NULL,
                carte_id_article INTEGER NOT NULL,
                name_article VARCHAR NOT NULL,
                type_id_article INTEGER NOT NULL,
                CONSTRAINT article_pk PRIMARY KEY (article_id)
);


CREATE TABLE pizzaocpublicschema.Stock (
                stock_id_stock INTEGER NOT NULL,
                produit_name_stock VARCHAR NOT NULL,
                produit_quantite_stock INTEGER NOT NULL,
                CONSTRAINT stock_pk PRIMARY KEY (stock_id_stock)
);


CREATE TABLE pizzaocpublicschema.article_stock_association_table (
                article_stock_association_id_table INTEGER NOT NULL,
                stock_id_assocaition INTEGER NOT NULL,
                article_id_association INTEGER NOT NULL,
                CONSTRAINT article_stock_association_table_pk PRIMARY KEY (article_stock_association_id_table, stock_id_assocaition, article_id_association)
);


CREATE TABLE pizzaocpublicschema.Pizzeria (
                pizzeria_id INTEGER NOT NULL,
                carte_id INTEGER NOT NULL,
                CONSTRAINT pizzeria_pk PRIMARY KEY (pizzeria_id)
);


CREATE TABLE pizzaocpublicschema.Employe (
                pizzeria_id_employe INTEGER NOT NULL,
                employe_id REAL NOT NULL,
                nom_employe VARCHAR NOT NULL,
                statut_employe VARCHAR NOT NULL,
                disponible_travail VARCHAR NOT NULL,
                CONSTRAINT employe_pk PRIMARY KEY (pizzeria_id_employe, employe_id)
);


CREATE TABLE pizzaocpublicschema.Mode_transport (
                transport_id_client INTEGER NOT NULL,
                mode_transport_livraison VARCHAR NOT NULL,
                CONSTRAINT mode_transport_pk PRIMARY KEY (transport_id_client)
);


CREATE TABLE pizzaocpublicschema.Client (
                client_id INTEGER NOT NULL,
                nom_client VARCHAR NOT NULL,
                prenom_client VARCHAR NOT NULL,
                CONSTRAINT client_pk PRIMARY KEY (client_id)
);


CREATE TABLE pizzaocpublicschema.Coordonnes (
                coordonnes_id_client INTEGER NOT NULL,
                telephone INTEGER NOT NULL,
                mail VARCHAR NOT NULL,
                client_id_coordonnes INTEGER NOT NULL,
                CONSTRAINT coordonnes_pk PRIMARY KEY (coordonnes_id_client)
);


CREATE TABLE pizzaocpublicschema.Adresse (
                adresse_id_table INTEGER NOT NULL,
                client_id_adresse INTEGER NOT NULL,
                code_postal INTEGER NOT NULL,
                ville VARCHAR NOT NULL,
                adresse VARCHAR NOT NULL,
                adresse_numero INTEGER NOT NULL,
                CONSTRAINT adresse_pk PRIMARY KEY (adresse_id_table)
);


CREATE TABLE pizzaocpublicschema.Panier (
                panier_id INTEGER NOT NULL,
                client_id_panier INTEGER NOT NULL,
                transport_id_panier INTEGER NOT NULL,
                CONSTRAINT panier_pk PRIMARY KEY (panier_id)
);


CREATE TABLE pizzaocpublicschema.Ligne_panier (
                ligne_panier_id INTEGER NOT NULL,
                panier_id_ligne_panier INTEGER NOT NULL,
                article_id_ligne_panier INTEGER NOT NULL,
                quantite_ligne_article INTEGER NOT NULL,
                CONSTRAINT ligne_panier_pk PRIMARY KEY (ligne_panier_id, panier_id_ligne_panier, article_id_ligne_panier)
);


ALTER TABLE pizzaocpublicschema.Article ADD CONSTRAINT type_article_fk
FOREIGN KEY (type_id_article)
REFERENCES pizzaocpublicschema.type (type_id_table)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Pizzeria ADD CONSTRAINT carte_pizzeria_fk
FOREIGN KEY (carte_id)
REFERENCES pizzaocpublicschema.Carte (carte_id_table)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Article ADD CONSTRAINT carte_article_fk
FOREIGN KEY (carte_id_article)
REFERENCES pizzaocpublicschema.Carte (carte_id_table)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Ligne_panier ADD CONSTRAINT article_ligne_panier_fk
FOREIGN KEY (article_id_ligne_panier)
REFERENCES pizzaocpublicschema.Article (article_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.article_stock_association_table ADD CONSTRAINT article_article_stock_association_table_fk
FOREIGN KEY (article_id_association)
REFERENCES pizzaocpublicschema.Article (article_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.article_stock_association_table ADD CONSTRAINT stock_article_stock_association_table_fk
FOREIGN KEY (stock_id_assocaition)
REFERENCES pizzaocpublicschema.Stock (stock_id_stock)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Employe ADD CONSTRAINT pizzeria_employe_fk
FOREIGN KEY (pizzeria_id_employe)
REFERENCES pizzaocpublicschema.Pizzeria (pizzeria_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Panier ADD CONSTRAINT mode_transport_panier_fk
FOREIGN KEY (transport_id_panier)
REFERENCES pizzaocpublicschema.Mode_transport (transport_id_client)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Panier ADD CONSTRAINT client_panier_fk
FOREIGN KEY (client_id_panier)
REFERENCES pizzaocpublicschema.Client (client_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Adresse ADD CONSTRAINT client_adresse_fk
FOREIGN KEY (client_id_adresse)
REFERENCES pizzaocpublicschema.Client (client_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Coordonnes ADD CONSTRAINT client_coordonnes_fk
FOREIGN KEY (client_id_coordonnes)
REFERENCES pizzaocpublicschema.Client (client_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE pizzaocpublicschema.Ligne_panier ADD CONSTRAINT panier_ligne_panier_fk
FOREIGN KEY (panier_id_ligne_panier)
REFERENCES pizzaocpublicschema.Panier (panier_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;