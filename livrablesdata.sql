--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.4 (Ubuntu 11.4-1.pgdg18.04+1)

-- Started on 2019-07-31 02:31:57 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3030 (class 0 OID 25489)
-- Dependencies: 204
-- Data for Name: client; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.client (client_id, nom_client, prenom_client) FROM stdin;
1	Macron	Naevius
2	Tibère	Claude
\.


--
-- TOC entry 3032 (class 0 OID 25505)
-- Dependencies: 206
-- Data for Name: adresse; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.adresse (adresse_id_table, client_id_adresse, code_postal, ville, adresse, adresse_numero) FROM stdin;
1	1	75008	Paris	rue Faubourg	55
2	2	75007	Paris	rue de Varenne	57
\.


--
-- TOC entry 3023 (class 0 OID 25442)
-- Dependencies: 197
-- Data for Name: carte; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.carte (carte_id_table) FROM stdin;
1
2
3
\.


--
-- TOC entry 3022 (class 0 OID 25434)
-- Dependencies: 196
-- Data for Name: type; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.type (type_id_table, name_type_article) FROM stdin;
1	pizza
2	boisson
3	entree
4	desert
\.


--
-- TOC entry 3024 (class 0 OID 25447)
-- Dependencies: 198
-- Data for Name: article; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.article (article_id, carte_id_article, name_article, type_id_article) FROM stdin;
1	1	Napolitaine	1
2	1	4 Formage	1
3	1	Tiramisu	4
4	1	coca	2
5	1	Aubergine gratinée	3
\.


--
-- TOC entry 3025 (class 0 OID 25455)
-- Dependencies: 199
-- Data for Name: stock; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.stock (stock_id_stock, produit_name_stock, produit_quantite_stock) FROM stdin;
1	pate pizza	10000
2	mozzarela	1000
\.


--
-- TOC entry 3026 (class 0 OID 25463)
-- Dependencies: 200
-- Data for Name: article_stock_association_table; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.article_stock_association_table (article_stock_association_id_table, stock_id_assocaition, article_id_association) FROM stdin;
1	1	1
2	2	2
\.


--
-- TOC entry 3031 (class 0 OID 25497)
-- Dependencies: 205
-- Data for Name: coordonnes; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.coordonnes (coordonnes_id_client, telephone, mail, client_id_coordonnes) FROM stdin;
1	102030508	finistere@ouest-france.fr	1
2	102061531	Claude.Tibere@elysee.fr	1
\.


--
-- TOC entry 3027 (class 0 OID 25468)
-- Dependencies: 201
-- Data for Name: pizzeria; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.pizzeria (pizzeria_id, carte_id) FROM stdin;
1	1
2	2
\.


--
-- TOC entry 3028 (class 0 OID 25473)
-- Dependencies: 202
-- Data for Name: employe; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.employe (pizzeria_id_employe, employe_id, nom_employe, statut_employe, disponible_travail) FROM stdin;
1	1	Jérémy Viale	cuisinier	oui
2	1	Frédéric Desmurs	cuisinier	oui
1	3	Frank Martin	livreur	oui
2	4	Sebastien loeb	livreur	non
\.


--
-- TOC entry 3029 (class 0 OID 25481)
-- Dependencies: 203
-- Data for Name: mode_transport; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.mode_transport (transport_id_client, mode_transport_livraison) FROM stdin;
1	livraison
2	emporter
\.


--
-- TOC entry 3033 (class 0 OID 25513)
-- Dependencies: 207
-- Data for Name: panier; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.panier (panier_id, client_id_panier, transport_id_panier) FROM stdin;
1	1	1
2	2	2
\.


--
-- TOC entry 3034 (class 0 OID 25518)
-- Dependencies: 208
-- Data for Name: ligne_panier; Type: TABLE DATA; Schema: pizzaocpublicschema; Owner: postgres
--

COPY pizzaocpublicschema.ligne_panier (ligne_panier_id, panier_id_ligne_panier, article_id_ligne_panier, quantite_ligne_article) FROM stdin;
1	1	1	1
2	2	2	2
\.


-- Completed on 2019-07-31 02:31:57 CEST

--
-- PostgreSQL database dump complete
--

